package com.example.unsplash

import androidx.appcompat.widget.SearchView
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.callbackFlow

fun SearchView.searchFlow(): kotlinx.coroutines.flow.Flow<String>{
    return callbackFlow {
        val queryTextListener = object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                TODO("Not yet implemented")
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                sendBlocking(newText.orEmpty())
                return true
            }
        }
        this@searchFlow.setOnQueryTextListener(queryTextListener)
        awaitClose{
            this@searchFlow.removeCallbacks{ queryTextListener }
        }
    }
}