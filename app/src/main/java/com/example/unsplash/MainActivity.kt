package com.example.unsplash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.flow.debounce

class MainActivity : AppCompatActivity(), Observable {

    private val liveData = MutableLiveData<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController =
            Navigation.findNavController(this, R.id.fragment)
        val bottomNavigationView =
            findViewById<BottomNavigationView>(R.id.bottomNav)
        NavigationUI.setupWithNavController(bottomNavigationView, navController)


        val searchView = toolBar.menu.findItem(R.id.search)

        //(searchView.actionView as androidx.appcompat.widget.SearchView).searchFlow()

        (searchView.actionView as androidx.appcompat.widget.SearchView).setOnQueryTextListener(
            object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (query != null) {
                        liveData.postValue(query)
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    liveData.postValue(newText ?: "" )
                    return true
                }
            })
    }

    override fun getLiveData(): LiveData<String> {
        return liveData
    }
}