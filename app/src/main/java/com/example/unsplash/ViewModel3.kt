package com.example.unsplash

import android.util.Log
import androidx.lifecycle.ViewModel

class ViewModel3: ViewModel() {

    override fun onCleared() {
        super.onCleared()
        Log.e("TAG", "ViewModel3 cleared")
    }
}