package com.example.unsplash

import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import kotlinx.android.synthetic.main.fragment_test2.*
import kotlinx.android.synthetic.main.fragment_test3.*

class TestFragment3: Fragment(R.layout.fragment_test3) {

    private val viewModel3 = ViewModel3()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e("TAG", "TestFragment3 attach")
    }

    override fun onStart() {
        super.onStart()
        Log.e("TAG", "TestFragment3 start")
    }

    override fun onResume() {
        super.onResume()
        Log.e("TAG", "TestFragment3 resume")
        (activity as Observable).getLiveData().observe(viewLifecycleOwner){
            tvTestFragment3.append(it)
        }
    }

    override fun onPause() {
        super.onPause()
        Log.e("TAG", "TestFragment3 pause")
    }

    override fun onStop() {
        super.onStop()
        Log.e("TAG", "TestFragment3 stop")
    }

}