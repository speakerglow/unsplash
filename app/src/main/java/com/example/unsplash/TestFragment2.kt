package com.example.unsplash

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_test2.*
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.util.concurrent.Flow

class TestFragment2: Fragment(R.layout.fragment_test2) {

    private val viewModel = ViewModel2()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e("TAG", "TestFragment2 attach")
    }

    override fun onStart() {
        super.onStart()
        Log.e("TAG", "TestFragment2 start")
    }

    override fun onResume() {
        super.onResume()
        Log.e("TAG", "TestFragment2 resume")
        (activity as Observable).getLiveData().observe(viewLifecycleOwner){
            tvTestFragment2.text = it
        }
    }

    override fun onPause() {
        super.onPause()
        Log.e("TAG", "TestFragment2 pause")
    }

    override fun onStop() {
        super.onStop()
        Log.e("TAG", "TestFragment2 stop")
    }

}