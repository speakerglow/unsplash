package com.example.unsplash

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_test1.*


class TestFragment1: Fragment(R.layout.fragment_test1){

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewPager()
       /* val searchView = toolBar.menu.findItem(R.id.search)

        (searchView.actionView as androidx.appcompat.widget.SearchView).setOnQueryTextListener(
            object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (query != null) {
                        liveData.postValue(query)
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }
            })*/
    }

    fun initViewPager() {
        val adapter = OnBoardingAdapter(listOf(TestFragment2(), TestFragment3()), this)

        viewPager.adapter = adapter

        viewPager.setPageTransformer(object : ViewPager2.PageTransformer {


            override fun transformPage(page: View, position: Float) {
                val percentage = 1 - Math.abs(position)
                page.cameraDistance = 12000f
                setVisibility(page, position)
                setTranslation(page)
                setSize(page, position, percentage)
                setRotation(page, position, percentage)
            }

            private fun setVisibility(page: View, position: Float) {
                if (position < 0.5 && position > -0.5) {
                    page.visibility = View.VISIBLE
                } else {
                    page.visibility = View.INVISIBLE
                }
            }

            private fun setTranslation(page: View) {
                val viewPager = page.parent.parent as ViewPager2
                val scroll = viewPager.scrollX - page.left
                page.translationX = scroll.toFloat()
            }

            private fun setSize(page: View, position: Float, percentage: Float) {
                page.setScaleX(if (position != 0f && position != 1f) percentage else 1f)
                page.setScaleY(if (position != 0f && position != 1f) percentage else 1f)
            }

            private fun setRotation(page: View, position: Float, percentage: Float) {
                if (position > 0) {
                    page.rotationY = -180 * (percentage + 1)
                } else {
                    page.rotationY = 180 * (percentage + 1)
                }
            }


            /*override fun transformPage(page: View, position: Float) {
                page.setCameraDistance(20000f)

                if (position < -1) {
                    page.setAlpha(0f);

                } else if (position <= 0) {
                    page.setAlpha(1f);
                    page.setPivotX(page.getWidth().toFloat());
                    page.setRotationY(90 * Math.abs(position));

                } else if (position <= 1) {
                    page.setAlpha(1f);
                    page.setPivotX(0f);
                    page.setRotationY(-90 * Math.abs(position));

                } else {
                    page.setAlpha(0f);
                }
            }
        */
        })
    }
}