package com.example.unsplash

import androidx.lifecycle.LiveData

interface Observable {
    fun getLiveData(): LiveData<String>
}