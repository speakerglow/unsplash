package com.example.unsplash

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class OnBoardingAdapter(private val screens: List<Fragment>, fragment: TestFragment1) :
        FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return screens.size
    }

    override fun createFragment(position: Int): Fragment {
        val item = screens[position]
        return item
    }
}